# log_uptime

## An uptime logger for Linux

The purpose of this script is to log all uptimes and downtimes of a machine. It's plain Bash script, needing only coreutils' `date`, `cut` and `tail`, and `awk` to function.

## What does it do?

`log_uptime` will log the current uptime each time when it's invoked. If the system had been shut down between two runs, `log_uptime` will detect that and log it accordingly. The package consists of two scripts:

* `log_uptime`: The actual uptime logger
* `parse_uptimelog`: Generates a human-readable output for the uptime log

## Setup

To install the package, put the two executables somewhere you want (e. g. in `/usr/local/bin/`) and create a directory where log_uptime's log will go.

The standard log file locations are:

    /var/log/uptime/current
    /var/log/uptime/uptime.log

These can be customized in `/etc/log_uptime` which is sourced by `log_uptime` and `parse_uptimelog`. Simply set the `uptimeFile` and `uptimeLog` Bash variables like so:

    uptimeFile="/some/other/current"
    uptimeLog="/some/other/uptime.log"

Be sure to have this file formatted correctly.

## Usage

Be sure to invoke `log_uptime` when the system finishes booting and before going down (see below).

If the system is intended to be up for a long time, it could be meaningful to also create a cronjob logging the current uptime from time to time, so we don't lose too much uptime if the system goes down by accident (e.g. by loss of electricity).

A cronjob logging the current uptime once an hour (using vixie-cron, cronie etc.) would e. g. be:

    0 * * * * root log_uptime

Automatic invocation when the system goes up and down can be achieved differently, depending on the init system in use. For OpenRC and Systemd, this can be done in the following way:

### OpenRC

On e.g. Gentoo/Artix/Devuan etc. using OpenRC, it's quite simple. you would add a `log_uptime` call to `/etc/local.d/local.start` and `/etc/local.d/local.stop` and you're done.

### Systemd

On a Systemd machine, it's a bit more complicated. You may want to create a "log_uptime" service. To achieve this, create a file called `log_uptime.service` in `/lib/systemd/system/` or `/usr/lib/systemd/system/` (depending on your distribution) with the following content (be sure to provide the correct path to `log_uptime`):

    [Unit]
    Description=Log uptime

    [Service]
    Type=oneshot
    RemainAfterExit=true
    ExecStart=/usr/local/bin/log_uptime
    ExecStop=/usr/local/bin/log_uptime

    [Install]
    WantedBy=multi-user.target

Enable and start the service (as root or via `sudo`) like this:

    # systemctl enable log_uptime
    # systemctl daemon-reload
    # systemctl start log_uptime

After that, `log_uptime` should be invoked when the system starts, and also if a halt/reboot is initiated.

## Update from pre-0.2 versions

The log format has changed in version 0.2. To convert the old log format used in older versions, you can simply run

    parse_uptimelog -pl | awk -F '[:-]' '{ print  $2, $3 }' > converted_log.log
