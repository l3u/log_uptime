.. SPDX-FileCopyrightText: 2021-2022 Tobias Leupold <tl at stonemx dot de>

   SPDX-License-Identifier: CC-BY-SA-4.0


   The format of this file is inspired by keepachangelog.com, but uses ReStructuredText instead of
   MarkDown. Keep the line length at no more than 100 characters (with the obvious exception of the
   header template below, which needs to be indented by three spaces)

   Here's the header template to be pasted at the top after a new release:

   ====================================================================================================
   [unreleased]
   ====================================================================================================

   Added
   =====

   * for new features.

   Changed
   =======

   * for changes in existing functionality.

   Deprecated
   ==========

   * for soon-to-be removed features.

   Removed
   =======

   * for now removed features.

   Fixed
   =====

   * for any bug fixes.

   Security
   ========

   * in case of vulnerabilities.

====================================================================================================
[unreleased]
====================================================================================================

Added
=====

* The current uptime and uptime log files can now be set via ``/etc/log_uptime``.

Changed
=======

* log_uptime is now licensed as "GPL v3 or later". Also switched to SPD-X license headers.

Deprecated
==========

* for soon-to-be removed features.

Removed
=======

* for now removed features.

Fixed
=====

* for any bug fixes.

Security
========

* in case of vulnerabilities.

====================================================================================================
log_uptime 0.2.3 released (05.10.2014)
====================================================================================================

* No ChangeLog present for older versions ;-)
